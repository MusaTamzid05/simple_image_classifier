using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Barracuda;
using System.Linq;
using System;


public class ClassifierCube : MonoBehaviour
{

    public WebCam webCam;

    const int IMAGE_SIZE = 224;

    const string INPUT_NAME = "input";
    const string OUTPUT_NAME = "output";

    public NNModel modelFile;
    public Preprocess preprocess;
    public TextAsset labelAsset;
    public bool faceFound;

    public Texture2D resizeTexture;

    string[] labels;
    IWorker worker;
    

    void Start()
    {
        var model = ModelLoader.Load(modelFile);
        preprocess = new Preprocess();
        worker = WorkerFactory.CreateWorker(WorkerFactory.Type.ComputePrecompiled, model);
        loadLabels();
        faceFound = false;
    }

    void Update()
    {

        WebCamTexture frame = webCam.getFrame();

        if(frame.didUpdateThisFrame && frame.width > 100) {
			resizeTexture = preprocess.ScaleAndCropImage(frame, IMAGE_SIZE);

            Renderer renderer = GetComponent<Renderer>();
            renderer.material.mainTexture = resizeTexture;
            byte[] bytes = resizeTexture.GetRawTextureData();
            RunModelRoutine(bytes);
        }
        
    }

    public Texture getTexture() {
        Renderer renderer = GetComponent<Renderer>();
        return renderer.material.mainTexture;

    }

    private void loadLabels() {
		var stringArray = labelAsset.text.Split('\'');
        labels = new string[(stringArray.Length - 1) / 2];
        int arrIndex = 0;

        for(int i = 0 ; i <  stringArray.Length; i += 1) {
            if(i % 2 != 0) {
                labels[arrIndex] = stringArray[i];
                arrIndex += 1;
            }
        }

        foreach(string label in labels) {
            Debug.Log("Label " + label);
        }

    }




	void RunModelRoutine(byte[] pixels) {

		Tensor tensor = TransformInput(pixels);

		var inputs = new Dictionary<string, Tensor> {
			{ INPUT_NAME, tensor }
		};

		worker.Execute(inputs);
		Tensor outputTensor = worker.PeekOutput(OUTPUT_NAME);

		List<float> temp = outputTensor.ToReadOnlyArray().ToList();
        int labelIndex =  (int)Math.Round(temp.Max());
        string labelStr = labels[labelIndex];
        //Debug.Log(labelStr);

        if(labelStr == "face")
            faceFound = true;


        tensor.Dispose();
		outputTensor.Dispose();
	}

	Tensor TransformInput(byte[] pixels){
		float[] transformedPixels = new float[pixels.Length];

		for (int i = 0; i < pixels.Length; i++){
			transformedPixels[i] = (pixels[i] - 127f) / 128f;
		}
		return new Tensor(1, IMAGE_SIZE, IMAGE_SIZE, 3, transformedPixels);
	}
}
