using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebCam : MonoBehaviour
{

    private WebCamTexture webcamTexture;

    void Start()
    {
        initCamera();
    }

    void Update()
    {

        
    }

    private void initCamera() {

        if(WebCamTexture.devices.Length == 0) {
            //Debug.Log("No camera was found");
            return;
        }

        webcamTexture = new WebCamTexture();

        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = webcamTexture;
        webcamTexture.Play();

    }

    public WebCamTexture getFrame() {
        return webcamTexture;
    }
}
