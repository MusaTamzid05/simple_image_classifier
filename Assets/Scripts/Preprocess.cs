using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using UnityEngine.Events;
using UnityEngine.Rendering;

public class Preprocess : MonoBehaviour {

    RenderTexture renderTexture;
    Vector2 scale = new Vector2(1, 1);
    Vector2 offset = Vector2.zero;

    UnityAction<byte[]> callback;

    public Texture2D ScaleAndCropImage(WebCamTexture webCamTexture, int desiredSize) {


        if (renderTexture == null) {
            renderTexture = new RenderTexture(desiredSize, desiredSize,0,RenderTextureFormat.ARGB32);
        }

        scale.x = (float)webCamTexture.height / (float)webCamTexture.width;
        offset.x = (1 - scale.x) / 2f;
        Graphics.Blit(webCamTexture, renderTexture, scale, offset);
        Texture2D result = toTexture2D(renderTexture);

        return result;
    }

    void OnCompleteReadback(AsyncGPUReadbackRequest request) {

        if (request.hasError) {
            Debug.Log("GPU readback error detected.");
            return;
        }

        callback.Invoke(request.GetData<byte>().ToArray());
    }

    public Texture2D toTexture2D(RenderTexture rTex) {
        Texture2D tex = new Texture2D(rTex.width, rTex.height, TextureFormat.RGB24, false);
        var oldRenderTexture = RenderTexture.active;
        RenderTexture.active = rTex;

        // It Reads from the active RenderTexture, we just need 
        // to give the coordinates.
        tex.ReadPixels(new Rect(0, 0, rTex.width, rTex.height), 0 ,0);
        tex.Apply();

        return tex;


    }
}
