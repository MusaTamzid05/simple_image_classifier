using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Barracuda;
using System.Linq;
using System;

public class HandClassifier : MonoBehaviour
{


    const string INPUT_NAME = "input";
    const string OUTPUT_NAME = "output";

    const int IMAGE_SIZE = 224;
    public ClassifierCube classfierCube;


    public NNModel modelFile;
    string[] labels;
    public TextAsset labelAsset;
    IWorker worker;


    
    void Start()
    {
        initModel();
        
    }

    void initModel() {

        var model = ModelLoader.Load(modelFile);
        worker = WorkerFactory.CreateWorker(WorkerFactory.Type.ComputePrecompiled, model);
        loadLabels();
    }


    void loadLabels() {
		var stringArray = labelAsset.text.Split('\'');
        labels = new string[(stringArray.Length - 1) / 2];
        int arrIndex = 0;

        for(int i = 0 ; i <  stringArray.Length; i += 1) {
            if(i % 2 != 0) {
                labels[arrIndex] = stringArray[i];
                arrIndex += 1;
            }
        }

        foreach(string label in labels) {
            Debug.Log("Label " + label);
        }

    }


    void Update()
    {

        if(classfierCube == null) {
            Debug.Log("Please set the classifer cube first");
            return;
        }


        if(classfierCube.faceFound) {
            updateCam();
            classify();


        } else {
            Debug.Log("Face not found");
        }
        
    }

    void updateCam() {
        Renderer renderer = GetComponent<Renderer>();
        renderer.material.mainTexture = classfierCube.getTexture();
    }


	Tensor TransformInput(byte[] pixels){
		float[] transformedPixels = new float[pixels.Length];

		for (int i = 0; i < pixels.Length; i++){
			transformedPixels[i] = (pixels[i] - 127f) / 128f;
		}
		return new Tensor(1, IMAGE_SIZE, IMAGE_SIZE, 3, transformedPixels);
	}


    void classify() {
        Texture2D texture = classfierCube.resizeTexture;
        byte[] bytes = texture.GetRawTextureData();

        Tensor tensor = TransformInput(bytes);


		var inputs = new Dictionary<string, Tensor> {
			{ INPUT_NAME, tensor }
		};

		worker.Execute(inputs);
		Tensor outputTensor = worker.PeekOutput(OUTPUT_NAME);

		List<float> temp = outputTensor.ToReadOnlyArray().ToList();

        int labelIndex =  (int)Math.Round(temp.Max());
        string labelStr = labels[labelIndex];

        Debug.Log(labels[labelIndex]);


        tensor.Dispose();
		outputTensor.Dispose();

    }

}
